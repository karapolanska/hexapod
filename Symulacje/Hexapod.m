classdef Hexapod < handle
    
    properties % Connection properties
        vrep = [];
        clientID = -1;
    end
    
    properties % Hexapod properties
    
        HexapodName = 'Mark_II';
        FootTipName = 'LegTipTarget_';
        BodyHandle;
        FootTipHandles;     
        
        FootTipInitialPosition = zeros(6,3);
        FootTipDesiredPosition = zeros(6,3);
    end
    
    properties % Gait properties
        
       %==================================
        %   Parametry ustalane
        %==================================
        Direction = 0; % kierunek chodu [stopnie]
        T = 1; % Okres całego cyklu ruchu nogi [s]
        B = 0.7; % Wspolczynnik podparcia [0-1]
        Ls = 0.14; % Dlugosc kroku [m]
        Hb = 0; % Wysokosc korpusu wzgledem pozycji poczatkowej [m]
        Fc = 0.04; % Wysokosc podniesienia stopy [m]
        FBc = 0.02; % odleglosc stopy od korpusu [m]
        % LF - Left front, RM - Right Middle itp.
        %               [LF,  RF,  LM,  RM, LR, RR] 
        LegStartPhase = [4/6, 1/6, 2/6, 5/6, 0, 3/6]; 
        %Przesuniecie w fazie [0 - 1] dla poszczegolnych nog. 
        
        
        %==================================
        %   Parametry wyliczane
        %==================================
        Vn = 0; % Predkosc Hexapoda [cm/s]
        Tb = 0; % Czas podparcia [s]
        Tp = 0; % Czas przenoszenia [s]  
        LegTimer = [0, 0, 0, 0, 0, 0];
        % Nogi jako wektor [LF, RF, LM, RM, LB, RB]
        
        ProtractionTimer = [0,0,0,0,0,0];
        RetractionTimer = [0,0,0,0,0,0];
    end
    
        %==================================
        %   Parametry symulacji
        %==================================
    properties % Simulation properties
        
        ActualWalkingTime = 0; % Aktualny czas chodu [s]
        SimulationStep = 0.01; % Krok czasu symulacji [s]
        SimulationPauseMultiplier = 0.5; % Mnoznik pauzy
        
    end
    
        %==================================
        %   Metody
        %==================================
    methods
        %+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        function obj = Hexapod(Name)
            obj.HexapodName = Name;
            obj.Vn = obj.Ls/obj.T; % % Predkosc Hexapoda [cm/s]
            obj.Tb = obj.T * obj.B; % Czas trwania fazy podparcia [s]
            obj.Tp = obj.T - obj.Tb; % Czas trwania fazy wysiegniecia [s]
        end
        
        % Konstruktor
        %+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        function result = InitConnection(obj)
            obj.vrep=remApi('remoteApi'); % using the prototype file (remoteApiProto.m)
            obj.vrep.simxFinish(-1); % just in case, close all opened connections
        	obj.clientID=obj.vrep.simxStart('127.0.0.1',19997,true,true,5000,5);
            result = (obj.clientID > -1);
           
            if (result)
              disp('Nawiazano polaczenie z symulacja w Vrep.');
              obj.vrep.simxStartSimulation(obj.clientID, obj.vrep.simx_opmode_oneshot);
              pause(1);
            else
                disp('Nie udalo sie nawiazac polaczenia z Vrep.');
            end    
        end
        
        % Inicjalizuje polaczenie z Vrepem
        %+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        function result = GetHandlers(obj)
                      % Hexapod Body
            [res,obj.BodyHandle]=obj.vrep.simxGetObjectHandle(obj.clientID,obj.HexapodName,obj.vrep.simx_opmode_oneshot_wait);
            if (res==obj.vrep.simx_return_ok)
                fprintf('Polaczono z Hexapodem\n');
            else
                fprintf('Nie udalo sie polaczyc z Hexapodem\n');
                result  = -1;
            end   
            % Hexapod Legs     
            for i=1:1:6
             name = [obj.FootTipName,int2str(i)];
            [res,obj.FootTipHandles(i)]=obj.vrep.simxGetObjectHandle(obj.clientID,name,obj.vrep.simx_opmode_oneshot_wait);
                    if (res==obj.vrep.simx_return_ok)
                        %fprintf('Udalo sie polaczyc z %s\n',name);
                    else
                        fprintf('Nie udalo sie polaczyc z %s\n',name);
                        result = -1;
                    end         
            end
            
        
        end
        
        % Pobiera uchwyty do korpusu i nog
        %+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        function result = GetPositions(obj)
            
            result = 1;
            for i=1:1:6
             [res, obj.FootTipInitialPosition(i,:)]=obj.vrep.simxGetObjectPosition(obj.clientID, obj.FootTipHandles(i), obj.vrep.sim_handle_parent, obj.vrep.simx_opmode_oneshot_wait);
             if (res ~= obj.vrep.simx_return_ok)
                 result = -1;
             end
            end
            if(result)
                disp('Pobrano pozycje poczatkowe odnozy');
                obj.FootTipDesiredPosition = obj.FootTipInitialPosition;
            end
        end
        
        % Pobiera pozycje koncowek odnozy wzgledem korpusu
        %+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        function GenerateFootTipPositionForNextIteration(obj, Leg) % Generuje kolejna pozycje w funkcji czasu globalnego
            
            X = 1;
            Y = 2;
            Z = 3;
            
            %Aktualny czas ktory uplynal od rozpoczecia chodu plus -(Faza
            %rozpoczecia chodu) i ograniczenie do (0 - T)
            obj.LegTimer(Leg) = mod((obj.ActualWalkingTime - obj.LegStartPhase(Leg)*obj.T),obj.T);
            
            %Jezeli Czas dla tej nogi jest wiekszy niz 0, mozna rozpoczac
            %poruszanie
            if(obj.LegTimer(Leg) >= 0)
                               
                %Jesli czas jest krotszy niz czas trwania fazy podparcia
                if(obj.LegTimer(Leg) < obj.Tb) %Faza podparcia
                       % obj.ProtractionTimer(Leg) = 0;
                       % obj.RetractionTimer(Leg) = (obj.RetractionTimer(Leg) + obj.SimulationStep)/obj.Tb;                    
                     
                    % Nowa pozycja w bok = Poczatkowa pozycja nogi + dodatkowa odleglosc w bok
                    obj.FootTipDesiredPosition(Leg, X) = obj.FootTipInitialPosition(Leg, X) + obj.FBc*(-1)^not(mod(Leg,2));
                    
                    % Nowa pozycja w osi Z - bez zmian w fazie podparcia
                    obj.FootTipDesiredPosition(Leg, Z) = obj.FootTipInitialPosition(Leg, Z);
                    
                    %Nowa pozycja wzdluz = Poczatkowa pozycja - pol
                    %dlugosci kroku + dlugosc kroku*(czas fazy podparcia[0-1])
                    obj.FootTipDesiredPosition(Leg, Y) = obj.FootTipInitialPosition(Leg, Y)-(obj.Ls/2) + obj.Ls * obj.LegTimer(Leg)/obj.Tb;           
                    
                else % Faza siegniecia
                    
                    % Wyliczenie pozycji nogi w poszczegolnych osiach                   
                    % Nowa pozycja w bok = Poczatkowa pozycja nogi + dodatkowa odleglosc w bok
                    obj.FootTipDesiredPosition(Leg, X) = obj.FootTipInitialPosition(Leg, X) + obj.FBc*(-1)^not(mod(Leg,2));
                    
                    % Nowa pozycja dla osi Y - wzdluz tulowia. Liniowo jak
                    % poprzednio
                    obj.FootTipDesiredPosition(Leg, Y) = obj.FootTipInitialPosition(Leg, Y)+(obj.Ls/2) - obj.Ls * ((obj.LegTimer(Leg) - obj.Tb)/obj.Tp);
                    
                    %Nowa pozycja w osi Z - wysokosc. Opisana funkca sinus
                    obj.FootTipDesiredPosition(Leg, Z) = obj.FootTipInitialPosition(Leg, Z) + obj.Fc * sin(((obj.LegTimer(Leg) - obj.Tb)/obj.Tp)*pi);
          
                end  
            end 
        end
        %+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        function SetFootTipPosition(obj,Leg)    
            obj.vrep.simxSetObjectPosition(obj.clientID, obj.FootTipHandles(Leg), obj.vrep.sim_handle_parent, obj.FootTipDesiredPosition(Leg,:), obj.vrep.simx_opmode_oneshot);  
        end
        %+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        function SetGaitParameters(obj, Direction, T, B, Ls, Hb, Fc, FBc)
            
            obj.Direction = Direction;
            obj.T = T;
            obj.B = B;
            obj.Ls = Ls;
            obj.Hb = Hb;
            obj.Fc = Fc;
            obj.FBc = FBc;
            obj.Vn = obj.Ls/obj.T;
        end  
        %+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        function Walk(obj,WalkingTime)
            
            obj.ActualWalkingTime = 0;
            
            for t = 0:obj.SimulationStep:WalkingTime
                pause(obj.SimulationPauseMultiplier * obj.SimulationStep);
                obj.ActualWalkingTime = t;
                
                    for Leg = 1 : 1 : 6
                        obj.GenerateFootTipPositionForNextIteration(Leg);
                        obj.SetFootTipPosition(Leg)                  
                    end    
            end    
        end  
        %+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    end
    
end
