% Aby ten przyklad zadzialal, nalezy skopiowac do tego folderu 3 pliki.
%
% 1. remoteApi.dll (Windows) albo remoteApi.so (Linux)
% Plik znajduje sie w sciezce:
%C:\Program Files (x86)\V-REP3\V-REP_PRO_EDU\programming\remoteApiBindings\lib\lib\64Bit
%
% 2. remApi.m 
% 3. remoteApi.dll
% Pliki znajduja sie w sciezce:
%C:\Program Files (x86)\V-REP3\V-REP_PRO_EDU\programming\remoteApiBindings\matlab\matlab
%
% Nastepnie nalezy uruchomic symulator V-Rep, otworzyc scene "Mark_II"
% uruchomic symulacje i dopiero wtedy odpalic skrypt matlaba.

clear all;
% Tworzy obiekt klasy Hexapod
Mark_II = Hexapod('Mark_II'); 
Mark_II.InitConnection();
Mark_II.GetHandlers();
Mark_II.GetPositions();
Mark_II.Walk(40);