# README #

Repozytorium zawiera wszystkie pliki związane z projektem robota kroczącego typu Hexapod, tworzonego w ramach koła naukowego KoNaR. 

## Członkowie Projektu ##

1. Karolina Polańska - Algorytmy
2. Magdalena Kaczorowska - Algorytmy
3. Grzegorz Henig - Algorytmy
4. Grzegorz Sękowski - Mechanika
5. Łukasz Michalczak - Elektronika
6. Łukasz Chojnacki - ROS
7. Krzysztof Dąbek - Programowanie
8. Bartłomiej Kurosz - Symulacje, algorytmy, koordynator projektu